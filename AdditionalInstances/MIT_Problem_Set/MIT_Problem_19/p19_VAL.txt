Type-checking aldehydereduction
...action passes type checking.
Type-checking aldolcondensation
...action passes type checking.
Type-checking alkylationofbetadicarbonyl
...action passes type checking.
Type-checking amidesynthesisfromacidchloride
...action passes type checking.
Type-checking amidesynthesisfromacidhalides
...action passes type checking.
Type-checking aromaticbromination
...action passes type checking.
Type-checking aromaticnitration
...action passes type checking.
Type-checking catalytichydrogenationofalkenes
...action passes type checking.
Type-checking catalytichydrogenationofnitriles
...action passes type checking.
Type-checking catalytichydrogenationofnitrogroup
...action passes type checking.
Type-checking claisencondensation
...action passes type checking.
Type-checking cyanidedisplacementofbromidesn2
...action passes type checking.
Type-checking cyanidedisplacementoftosylgroupsn2
...action passes type checking.
Type-checking cyanohydrinformation
...action passes type checking.
Type-checking decarboxylationofbetaketoesters
...action passes type checking.
Type-checking dehydrationofamides
...action passes type checking.
Type-checking diazotization
...action passes type checking.
Type-checking dieckmanncyclization
...action passes type checking.
Type-checking dielsaldercycloaddition
...action passes type checking.
Type-checking enolateattackonalkylhalidesn2
...action passes type checking.
Type-checking enolatecalkylation
...action passes type checking.
Type-checking enolateformation
...action passes type checking.
Type-checking fischeresterification
...action passes type checking.
Type-checking friedelcraftsacylation
...action passes type checking.
Type-checking gabrielsynthesis
...action passes type checking.
Type-checking grignardadditiontoacidchlorides
...action passes type checking.
Type-checking grignardreaction
...action passes type checking.
Type-checking grignardreagentformation
...action passes type checking.
Type-checking hydrationofaldehydesandketones
...action passes type checking.
Type-checking hydrationofalkenes
...action passes type checking.
Type-checking hydrolysisofnitriles
...action passes type checking.
Type-checking hydroxylsubstitutionofdiazoniumionsn1
...action passes type checking.
Type-checking imineformation
...action passes type checking.
Type-checking iminereductiontoamine
...action passes type checking.
Type-checking intramolecularoxymercurationreduction
...action passes type checking.
Type-checking lahreductionofanhydrides
...action passes type checking.
Type-checking lahreductionofaldehydesandketones
...action passes type checking.
Type-checking lahreductionofnitriles
...action passes type checking.
Type-checking michaeladditiontounsaturatedketones
...action passes type checking.
Type-checking nitrilesubstitutionofdiazoniumionsn1
...action passes type checking.
Type-checking oxidationofalcoholswithpcc
...action passes type checking.
Type-checking oxidationofprimaryalcoholwithpotassiumpermanganate
...action passes type checking.
Type-checking paalknorrpyrrolesynthesis
...action passes type checking.
Type-checking pbr3conversionofalcoholstoalkylbromides
...action passes type checking.
Type-checking reductiveozonolysis
...action passes type checking.
Type-checking sandmeyerreaction
...action passes type checking.
Type-checking sodiumdichromateoxidationofprimaryalcohol
...action passes type checking.
Type-checking sodiumdichromateoxidationofsecondaryalcohol
...action passes type checking.
Type-checking stetterreaction
...action passes type checking.
Type-checking thionylchlorideconversionofcarboxylicacidstoacidchlorides
...action passes type checking.
Type-checking tosylationofalcohols
...action passes type checking.
Type-checking williamsonethersynthesis
...action passes type checking.
Checking plan: p19plan
Plan to validate:

Plan size: 2
1:
(cyanohydrinformation o1 c3 c2 h54 n1 c5 c1)
 
2:
(lahreductionofnitriles h22 o50 al2 o51 n1 h23 h50 c3 h52 li1 h51 h53 h24 h25 c2)
 

Plan Validation details
-----------------------

Checking next happening (time 1)
Deleting (bond h54 c3)
Deleting (bond c3 h54)
Deleting (doublebond o1 c2)
Deleting (doublebond c2 o1)
Adding (bond c3 c2)
Adding (bond c2 c3)
Adding (bond o1 h54)
Adding (bond h54 o1)
Adding (bond o1 c2)
Adding (bond c2 o1)

Checking next happening (time 2)
Deleting (bond h23 al2)
Deleting (bond al2 h23)
Deleting (bond h22 al2)
Deleting (bond al2 h22)
Deleting (triplebond n1 c3)
Deleting (triplebond c3 n1)
Deleting (bond h50 o50)
Deleting (bond o50 h50)
Deleting (bond h52 o51)
Deleting (bond o51 h52)
Adding (bond al2 o51)
Adding (bond o51 al2)
Adding (bond li1 o50)
Adding (bond o50 li1)
Adding (bond h23 c3)
Adding (bond c3 h23)
Adding (bond h22 c3)
Adding (bond c3 h22)
Adding (bond n1 c3)
Adding (bond c3 n1)
Adding (bond h50 n1)
Adding (bond n1 h50)
Adding (bond h52 n1)
Adding (bond n1 h52)
Plan executed successfully - checking goal
Plan valid
Final value: 2 

Successful plans:
Value: 2
 p19plan 2 

