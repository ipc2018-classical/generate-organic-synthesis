Type-checking additionofrohacrossgemdisubstitutedalkene
...action passes type checking.
Type-checking additionofrohacrossmonosubstitutedalkene
...action passes type checking.
Type-checking additionofrohacrosstetrasubstitutedalkene
...action passes type checking.
Type-checking additionofrohacrosstrisubstitutedalkene
...action passes type checking.
Type-checking additionofrohacrossvicdisubstitutedalkene
...action passes type checking.
Type-checking etherformationbysulfonatedisplacement
...action passes type checking.
Type-checking hydroborationofdiortrisubstitutedalkene
...action passes type checking.
Type-checking hydroborationofgemdisubstitutedalkene
...action passes type checking.
Type-checking hydroborationofmonosubstitutedalkene
...action passes type checking.
Type-checking hydroborationoftetrasubstitutedalkene
...action passes type checking.
Type-checking oxidationofborane
...action passes type checking.
Type-checking sulfonylationofalcohol
...action passes type checking.
Checking plan: p18plan
;Plan to validate:

Plan size: 2
1:
(sulfonylationofalcohol h180 o178 s072 cl073 c032 o068 c021 o069)
 
2:
(etherformationbysulfonatedisplacement h061 o066 c032 o178 o068 s072 h133 c021 o069 c033 h134 c016)
 

Plan Validation details
-----------------------

Checking next happening (time 1)
Deleting (bond o178 h180)
Deleting (bond h180 o178)
Deleting (bond cl073 s072)
Deleting (bond s072 cl073)
Adding (bond o178 s072)
Adding (bond s072 o178)
Adding (bond h180 cl073)
Adding (bond cl073 h180)

Checking next happening (time 2)
Deleting (bond c032 o178)
Deleting (bond o178 c032)
Deleting (bond h061 o066)
Deleting (bond o066 h061)
Adding (bond c032 o066)
Adding (bond o066 c032)
Adding (bond o178 h061)
Adding (bond h061 o178)
Plan executed successfully - checking goal
Plan valid
Final value: 2 

Successful plans:
Value: 2
 p18plan 2 

