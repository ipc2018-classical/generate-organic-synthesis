Type-checking additionofrohacrossgemdisubstitutedalkene
...action passes type checking.
Type-checking additionofrohacrossmonosubstitutedalkene
...action passes type checking.
Type-checking additionofrohacrosstetrasubstitutedalkene
...action passes type checking.
Type-checking additionofrohacrosstrisubstitutedalkene
...action passes type checking.
Type-checking additionofrohacrossvicdisubstitutedalkene
...action passes type checking.
Type-checking etherformationbysulfonatedisplacement
...action passes type checking.
Type-checking hydroborationofdiortrisubstitutedalkene
...action passes type checking.
Type-checking hydroborationofgemdisubstitutedalkene
...action passes type checking.
Type-checking hydroborationofmonosubstitutedalkene
...action passes type checking.
Type-checking hydroborationoftetrasubstitutedalkene
...action passes type checking.
Type-checking oxidationofborane
...action passes type checking.
Type-checking sulfonylationofalcohol
...action passes type checking.
Checking plan: alkene_p13_plan
Plan to validate:

Plan size: 1
1:
(additionofrohacrosstetrasubstitutedalkene c013 o063 c011 h057 c014 c010 c012 c015 h056)
 

Plan Validation details
-----------------------

Checking next happening (time 1)
Deleting (doublebond c013 c011)
Deleting (doublebond c011 c013)
Deleting (bond o063 h057)
Deleting (bond h057 o063)
Adding (bond c013 c011)
Adding (bond c011 c013)
Adding (bond c013 o063)
Adding (bond o063 c013)
Adding (bond c011 h057)
Adding (bond h057 c011)
Plan executed successfully - checking goal
Plan valid
Final value: 1 

Successful plans:
Value: 1
 alkene_p13_plan 1 

